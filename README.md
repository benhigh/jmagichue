# jMagicHue

This is a Java library for programmatically controlling MagicHue lights. In effect, this is an adaptation of [python-magichue](https://pypi.org/project/python-magichue/), just written for Java. All of the bulb commands come from that project, so massive kudos to the author, [namacha](https://github.com/namacha/). There are a few caveats and issues, as well as features I'm still wanting to add. For basic controls, however, this library is simple and easy to use.

## Compiling
Compilation is done through Gradle. Once you've cloned the repo, simply run `./gradlew clean build`. The resulting .jar file can be found in `build/libs`.

## Usage
Once the .jar file has been added to your classpath, you will have access to 2 classes: `Light` and `Status`.

- The `Light` class represents a MagicHue light, and provides methods for reading and modifying the light's status.
- The `Status` class is a data class that holds the on/off status, white light brightness, RGB values, and the toggle for the white light. This class does not contain any methods that actually **do** anything, but can be used for presets and is made up of primitive properties, so it can be easily serialized.

In order to connect to a light, you must know its IP address. Once you have that, you can instantiate a `Light` object, passing the IP address as a String. Optionally, if the light uses a different port than the default of `5577`, an extra constructor is present that you can use to specify that.

Once you have the `Light` object initialized, you can either manually override the `Status` object within by using `setStatus(Status status)`, or you can use the macro functions. They are:
- `setOn(boolean on)`
- `setRgb(int red, int green, int blue)`
- `setWhite(int white, boolean useWhite)`

The integer values should be between 0 and 255, but this restriction is not enforced by the library. Neither setting the status directly nor using a macro function will automatically write to the bulb. They will simply set the `status` property of the object, preparing it to send the data to the bulb.

Before reading or writing to the bulb, you must first connect to the bulb. You can do this using the `connect()` method. This may throw an `IOException`. See "Caveats" below for common connection problems.

Once connected, you can then use the following methods to send data to the bulb:
- `sendOnOff()` must be used if changing the on/off status of the bulb.
- `sendColorData()` must be used if changing the color of the bulb.

These calls may throw an `IOException`. If the bulb is not connected, these calls may also throw a `NotConnectedException`. See "Caveats" below for common issues sending data to some bulbs.

To query the bulb for its current status, use the `updateStatus()` method. This will update the `status` property on the object, and return a `Status` object. This method may throw an `IOException` or `NotConnectedException`. Additionally, if the method receives bad data from the bulb, it will throw a `ReadErrorException`.

Finally, to disconnect from the bulb, use the `disconnect()` method. This is recommended, but not necessarily required. After calling disconnect, the read/write methods will throw a `NotConnectedException` until `connect()` is called again.

## Example

```java
import java.io.IOException;
import xyz.benhigh.jmagichue.*;

public class Test {
    public static void main(String[] args) {
        try {
            // Connect to the light
            Light l = new Light("192.168.1.141");
            l.connect();

            // Read the current status
            Status status = l.updateStatus();
            System.out.println(status.toString());

            // Turn the light on
            l.setOn(true);
            l.sendOnOff();
            Thread.sleep(2000);

            // Set the color
            l.setRgb(255, 128, 64);
            l.setWhite(0, false); // Ensure white light is off
            l.sendColorData();
            Thread.sleep(2000);

            // Set the white light
            l.setWhite(255, true);
            l.sendColorData();
            Thread.sleep(2000);

            // Turn the light off
            l.setOn(false);
            l.sendOnOff();

            // Disconnect from the light
            l.disconnect();
        } catch (IOException | ReadErrorException | NotConnectedException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

## Caveats

- Bulbs cannot be connected to over LAN. This appears to be an issue with the bulbs themselves, but I'm still investigating ways to resolve this issue. Attempting to connect to a bulb over LAN will result in a "No route to host" `IOException`.
- Some bulbs will need a delay between write calls. I've found that around 500 ms works pretty well. This may only affect the bulbs I have, so the library will not automatically include a delay.
- Enabling the white light will only work if the white value is above 0. If the white value is 0, the colored lights will remain on until the white value is set to a value over 0. Once the white light has been enabled with a white value over 0, however, the white value can be set back to 0.
- Some bulbs appear to go to "sleep" after a certain amount of time, and will not respond to write requests. The only way I have managed to get around this is by turning the light off and then back on at the power source, or using the official app. Querying the lights before writing may also work, but I have not been able to test this.

## Future Features

- Reading and writing modes

## Tested Bulbs

The following are the bulbs this library has been tested. If you have any other bulbs that this library either works or does not work with, please let me know. Bulbs listed as "Works completely" may still be subject to the issues detailed under "Caveats".

- [BERENNIS Smart Light Bulb](https://www.amazon.com/BERENNIS-Dimmable-Multicolored-Required-Compatible/dp/B07GGWNQT7) - Works completely
