package xyz.benhigh.jmagichue;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Class to read and control a MagicHue light. Provides methods for getting and setting bulb data.
 *
 * @author Ben High
 */
public class Light {
	private String addr;
	private int port;
	private Socket socket;

	private Status status;

	/**
	 * Construct a Light object using a given IP address and the default port (5577). This will not connect to the
	 * light. See {@link #connect()}.
	 *
	 * @param addr The IP address of the light.
	 */
	public Light(String addr) {
		this(addr, 5577);
	}

	/**
	 * Construct a Light object using a given IP address and a given port. This will not connect to the light. See
	 * {@link #connect()}.
	 *
	 * @param addr The IP address of the light.
	 * @param port The port the light will receive data through.
	 */
	public Light(String addr, int port) {
		this.addr = addr;
		this.port = port;
		this.status = new Status();
	}

	/**
	 * Connect to the light. This method must be run before executing any other read/write method.
	 *
	 * @throws IOException Thrown if a network error occurs (connection refused, no route to host, etc).
	 */
	public void connect() throws IOException {
		socket = new Socket(addr, port);
	}

	/**
	 * Disconnect from the light. This method closes the socket connection and prevents other read/write methods from
	 * being executed until {@link #connect()} is called again.
	 *
	 * @throws IOException Thrown if a network error occurs.
	 */
	public void disconnect() throws IOException {
		socket.close();
		socket = null;
	}

	/**
	 * Override the status object entirely. This does not send an update to the light. See {@link #sendOnOff()} and
	 * {@link #sendColorData()}.
	 *
	 * @param status The {@link Status} object to use.
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Set the light to be turned on or off. This does not send an update to the light. See {@link #sendOnOff()}.
	 *
	 * @param on True if the light should be turned on. False if it should be turned off.
	 */
	public void setOn(boolean on) {
		this.status.setOn(on);
	}

	/**
	 * Set the red, green, and blue values. The values should be between 0 and 255, inclusively. This does not send an
	 * update to the light. See {@link #sendColorData()}. This method does not turn off white mode, which overrides
	 * colors. See {@link #setWhite(int, boolean)}.
	 *
	 * @param red   The red value of the bulb.
	 * @param green The green value of the bulb.
	 * @param blue  The blue value of the bulb.
	 */
	public void setRgb(int red, int green, int blue) {
		this.status.setRed(red);
		this.status.setGreen(green);
		this.status.setBlue(blue);
	}

	/**
	 * Enables or disables white mode, and sets the brightness. The brightness value should be between 0 and 255. This
	 * does not send an update to the light. See {@link #sendColorData()}. When white mode is enabled, the white value
	 * must be above 0, or else enabling white mode will not override color mode. After white mode is enabled, however,
	 * the white value can go to 0.
	 *
	 * @param white    The brightness of the white light.
	 * @param useWhite True to use the white light. False to use the colored lights.
	 */
	public void setWhite(int white, boolean useWhite) {
		this.status.setWhite(white);
		this.status.setUseWhite(useWhite);
	}

	/**
	 * Sends data to the bulb, with a checksum. The checksum is appended to the end of the bytes being sent to the
	 * light, and is equal to the decimal value of the last 2 characters of the hex value of the sum of all values in
	 * the data array.
	 *
	 * Before this method can be called, {@link #connect()} must first be called. If this method is called before then,
	 * it will throw a {@link NotConnectedException}.
	 *
	 * @param data The data to be sent to the bulb.
	 * @throws IOException Thrown if a network error occurs.
	 * @throws NotConnectedException Thrown if {@link #connect()} is not called before this method.
	 */
	private void sendData(int[] data) throws IOException, NotConnectedException {
		// Ensure we're connected to a light
		if (socket == null) {
			throw new NotConnectedException();
		}

		// Calculate the checksum
		int sum = 0;
		for (int item : data) {
			sum += item;
		}
		String hex = Integer.toHexString(sum);
		hex = hex.substring(hex.length() - 2);

		// Assemble the data
		ByteBuffer buffer = ByteBuffer.allocate(data.length + 1);
		for (int item : data) {
			buffer.put((byte) item);
		}
		buffer.put((byte) Integer.parseInt(hex, 16));

		// Send the data
		OutputStream stream = socket.getOutputStream();
		stream.write(buffer.array());
		stream.flush();
	}

	/**
	 * Send the on/off signal to the bulb.
	 *
	 * Data sent:
	 *  - 0x71: The command to tell the bulb we want to set the on/off status.
	 *  - 0x23 or 0x24: On or off, respectively.
	 *  - 0x0f: Terminator byte.
	 *
	 * @throws IOException Thrown if a network error occurs.
	 * @throws NotConnectedException Thrown if {@link #connect()} is not called before this method.
	 */
	public void sendOnOff() throws IOException, NotConnectedException {
		sendData(new int[] {Commands.SET_ON,
				this.status.isOn() ? Commands.ON : Commands.OFF,
				Commands.TERMINATE});
	}

	/**
	 * Send the color data to the bulb.
	 *
	 * Data sent:
	 *  - 0x31: The command to tell the bulb we want to set the color.
	 *  - The red value.
	 *  - The green value.
	 *  - The blue value.
	 *  - The white value.
	 *  - 0x0f or 0xf0: True or false for using the white light, respectively.
	 *  - 0x0f: Terminator byte.
	 *
	 * @throws IOException Thrown if a network error occurs.
	 * @throws NotConnectedException Thrown if {@link #connect()} is not called before this method.
	 */
	public void sendColorData() throws IOException, NotConnectedException {
		sendData(new int[] {Commands.SET_COLOR,
				status.getRed(),
				status.getGreen(),
				status.getBlue(),
				status.getWhite(),
				status.isUseWhite() ? Commands.TRUE : Commands.FALSE,
				Commands.TERMINATE});
	}

	/**
	 * Retrieve the status from the bulb. Sets the status on this object, and returns the status.
	 *
	 * This method sends 0x81, 0x8a, and 0x8b to the bulb to tell it to retrieve the data.
	 *
	 * Response (14 bytes):
	 *  Byte 0: Flag to show the read was successful (0x81 expected)
	 *  Byte 1: Unknown (always 0x44)
	 *  Byte 2: On or off (0x23 or 0x24, respectively)
	 *  Byte 3: Mode (currently unused)
	 *  Byte 4: Unknown (always 0x1)
	 *  Byte 5: Unknown (possibly mode speed? Dependent on the mode)
	 *  Byte 6: Red value
	 *  Byte 7: Green value
	 *  Byte 8: Blue value
	 *  Byte 9: White brightness
	 *  Byte 10: Unknown (always 0x9)
	 *  Byte 11: Unknown (always 0x0)
	 *  Byte 12: Unknown (0x23 or 0x24, possibly flag for mode vs. solid color?)
	 *  Byte 13: Unknown (possibly mode progress?)
	 *
	 * @return The parsed {@link Status} object.
	 * @throws IOException Thrown if a network error occurs.
	 * @throws NotConnectedException Thrown if {@link #connect()} is not called before this method.
	 * @throws ReadErrorException Thrown if an invalid number of bytes are received, or the success byte is not 0x81.
	 */
	public Status updateStatus() throws IOException, NotConnectedException, ReadErrorException {
		// Send a query command to the bulb
		sendData(new int[] {Commands.QUERY_1,
				Commands.QUERY_2,
				Commands.QUERY_3});

		// Read the response
		InputStream is = socket.getInputStream();
		byte[] bytes = new byte[14];
		int size = is.read(bytes);

		// Java uses signed bytes, but the bulb uses unsigned, so convert to ints and reverse overflow
		int[] result = new int[14];
		for (int i = 0; i < bytes.length; i ++) {
			result[i] = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
		}

		// Throw an error if an insufficient number of bytes were returned, or the success byte is invalid
		if (size != 14 || result[0] != Commands.READ_VALID) {
			throw new ReadErrorException(size != 14 ?
					"Invalid number of bytes received from the bulb." :
					"Query command was rejected by the bulb.");
		}

		// Parse the response into a status object, set it on this light, then return that status
		Status currentStatus = new Status();
		currentStatus.setOn(result[2] == Commands.ON);
		currentStatus.setRed(result[6]);
		currentStatus.setGreen(result[7]);
		currentStatus.setBlue(result[8]);
		currentStatus.setWhite(result[9]);
		currentStatus.setUseWhite(result[12] == Commands.TRUE);
		this.status = currentStatus;
		return currentStatus;
	}
}
