package xyz.benhigh.jmagichue;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Data class to hold the red, green, blue, and white values, as well as if the bulb is on or off, and if the bulb is
 * using the white light as opposed to the RGB lights.
 *
 * @author Ben High
 */
@Getter
@Setter
@NoArgsConstructor
public class Status implements Serializable {
	private static final long serialVersionUID = 8104085980859761532L;

	private boolean on;

	private int red;
	private int green;
	private int blue;

	private int white;
	private boolean useWhite;

	/**
	 * Constructor to set the red, green, and blue levels, and disable the white light.
	 *
	 * @param red   The red value.
	 * @param green The green value.
	 * @param blue  The blue value.
	 */
	public Status(int red, int green, int blue) {
		this.on = true;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.white = 0;
		this.useWhite = false;
	}

	/**
	 * Constructor to set the white value, and enable the white light. See the documentation for
	 * {@link Light#setWhite(int, boolean)} for information about caveats to white light mode.
	 *
	 * @param white The white brightness value.
	 */
	public Status(int white) {
		this.on = true;
		this.red = 0;
		this.green = 0;
		this.blue = 0;
		this.white = white;
		this.useWhite = true;
	}

	@Override
	public String toString() {
		return "Status{" +
				"on=" + on +
				", red=" + red +
				", green=" + green +
				", blue=" + blue +
				", white=" + white +
				", useWhite=" + useWhite +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Status status = (Status) o;
		return on == status.on &&
				red == status.red &&
				green == status.green &&
				blue == status.blue &&
				white == status.white &&
				useWhite == status.useWhite;
	}

	@Override
	public int hashCode() {
		return Objects.hash(on, red, green, blue, white, useWhite);
	}
}
