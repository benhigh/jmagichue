package xyz.benhigh.jmagichue;

/**
 * Exception to be thrown if attempting to read or write to a bulb before {@link Light#connect()} is called.
 *
 * @author Ben High
 */
public class NotConnectedException extends Exception {
	NotConnectedException() {
		super("Bulb is not connected. Please call .connect() before attempting to read or write to the bulb.");
	}
}
