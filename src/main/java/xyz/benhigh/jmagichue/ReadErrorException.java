package xyz.benhigh.jmagichue;

/**
 * Exception to be thrown when a read error has occurred during a query to the bulb. This can either be because the bulb
 * sent back an invalid number of bytes, or the bulb has indicated that the query command was malformed.
 *
 * @author Ben High
 */
class ReadErrorException extends Exception {
	ReadErrorException(String message) {
		super(message);
	}
}
