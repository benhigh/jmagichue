package xyz.benhigh.jmagichue;

/**
 * Class to provide constants used for reading and writing to a bulb.
 *
 * @author Ben High
 */
class Commands {
	static int TRUE = 0x0f;
	static int FALSE = 0xf0;

	static int ON = 0x23;
	static int OFF = 0x24;

	static int SET_ON = 0x71;
	static int SET_COLOR = 0x31;

	static int QUERY_1 = 0x81;
	static int QUERY_2 = 0x8a;
	static int QUERY_3 = 0x8b;

	static int TERMINATE = 0x0f;
	static int READ_VALID = 0x81;
}
